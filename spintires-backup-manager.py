#!/usr/bin/env python3

# REMEMBER TO SET THE VARIABLES "SAVES_SOURCE_DIR" and "SAVES_BACKUP_DIR"

# How To:
# python spintires-backup-manager.py --help
# ======================================================================================================================

import argparse
from datetime import date, datetime
import logging
from logging import debug, error, info, warn
import os
from pathlib import Path
import psutil
import shutil
import sys
from typing import List, Optional


# REMEMBER TO SET THESE VARIABLES (IMPORTANT: USE THE `Path('')` SYNTAX AS SHOWN BELOW!)
SAVES_SOURCE_DIR = None
SAVES_BACKUP_DIR = None
#
# EXAMPLE:
#SAVES_SOURCE_DIR = Path('/home/user/.local/share/Steam/steamapps/compatdata/263280/pfx/drive_c/users/steamuser/Application Data/SpinTires')
#SAVES_BACKUP_DIR = Path('/home/user/games/spintires/saves/')


class Backup():
    INTERNAL_RESTORESAVES_NAME = 'INTERNAL.RESTORESAVES'

    def __init__(self, path: Path, **kwargs):
        self._path = path

        dirname = path.name
        segments = dirname.split('-')
        # TODO: Handle parsing errors due to malformed directory names (if user modified them)
        self.date = date.fromisoformat('-'.join(segments[0:3]))
        self.name = '-'.join(segments[3:])

        revisions = [BackupRevision(path / subdir) for subdir in os.listdir(path)]
        self.revisions = sorted(revisions, key=lambda revision: revision.time)

    def __repr__(self):
        return f'Backup(_path="{self._path}", name="{self.name}", date={self.date}, revisions={self.revisions})'

    def __str__(self):
        return self.__repr__()

    def create_new_revision(self, name: Optional[str], source_path: Path) -> bool:
        if name:
            if any(map(lambda revision: revision.name == name, self.revisions)):
                error("Revision with the given name already exists. Aborting!")
                return False
        else:
            # if no specific revision name was requested, we will automatically generate a new name.
            # Automatically assigned names are "saveXX" where XX is an increasing number with leading zero, starting at
            # one
            highest_save_numer = 0
            for revision in self.revisions:
                revision_name = revision.name
                if revision_name.startswith('save'):
                    # we need this try block here, in case the user has already specified a revision name starting with save
                    try:
                        save_number = int(revision_name[4:])
                        highest_save_numer = max(highest_save_numer, save_number)
                    except ValueError:
                        pass
            highest_save_numer += 1
            name = f'save{highest_save_numer:02}'

        revision = BackupRevision.new(self._path, name, source_path)
        debug(revision)

        self.revisions.append(revision)
        return True

    def delete(self):
        shutil.rmtree(self._path)

    def get_latest_revision(self) -> 'BackupRevision':
        return self.revisions[-1]

    def get_revision(self, name: str) -> Optional['BackupRevision']:
        try:
            revision = next(filter(lambda revision: revision.name == name, self.revisions))
            return revision
        except StopIteration:
            return None

    @classmethod
    def get_from_list(cls, backups: List['Backup'], name: str) -> Optional['Backup']:
        try:
            return next(filter(lambda backup: backup.name == name, backups))
        except StopIteration:
            return None

    @classmethod
    def get_from_list_or_create(cls, backups: List['Backup'], name: str, path: Path) -> 'Backup':
        try:
            return next(filter(lambda backup: backup.name == name, backups))
        except StopIteration:
            return Backup.new(name, path)

    def is_internal(self) -> bool:
        return self.name == Backup.INTERNAL_RESTORESAVES_NAME

    @classmethod
    def load_list_from_path(cls, path: Path) -> List['Backup']:
        backups = [Backup(path / subdir) for subdir in os.listdir(path)]
        return sorted(backups, key=lambda backup: backup.date)

    @classmethod
    def new(cls, name: str, basepath: Path) -> 'Backup':
        backup_path = basepath / f'{date.today()}-{name}'
        backup_path.mkdir()

        return Backup(backup_path)


    def pretty_print(self, include_revisions: bool = False, even_if_internal: bool = False):
        extra_info = ''
        if self.is_internal():
            if even_if_internal:
                extra_info = ' (Note: This backup automatically creates a new revision whenever you restore another backup)'
            else:
                return

        print(f'[{self.date}] {self.name}{extra_info}')
        if include_revisions:
            for revision in self.revisions:
                revision.pretty_print(indent=4)


class BackupRevision():
    def __init__(self, path: Path, **kwargs):
        self._path = path

        segments = path.name.split('-')
        self.time = datetime(
            year=   int(segments[0]),
            month=  int(segments[1]),
            day=    int(segments[2]),
            hour=   int(segments[3][0:2]),
            minute= int(segments[3][2:4]),
        )
        self.name = '-'.join(segments[4:])

    def __repr__(self):
        return f'BackupRevision(_path="{self._path}", time={self.time}, name="{self.name}")'

    def __str__(self):
        return self.__repr__()

    def delete(self):
        shutil.rmtree(self._path)

    @classmethod
    def new(cls, in_path: Path, name: str, source_path: Path) -> 'BackupRevision':
        now = datetime.now()
        revision_path = in_path / f'{now.strftime("%Y-%m-%d-%H%M")}-{name}'
        debug(revision_path)

        shutil.copytree(source_path, revision_path)
        return BackupRevision(revision_path)

    def pretty_print(self, indent: int = 0):
        indent = ' ' * indent
        print(f'{indent}[{self.time}] {self.name}')

    def restore(self, target_dir: Path):
        shutil.rmtree(target_dir)
        shutil.copytree(self._path, target_dir)


# Main function of the application
def main():
    args = parse_args()
    logging.basicConfig(level=getattr(logging, args.log_level.upper()))

    debug(args)

    startup_checks()
    args.func(args)

def startup_checks():
    if not SAVES_BACKUP_DIR or not SAVES_SOURCE_DIR:
        error("Please set the variables SAVES_BACKUP_DIR and SAVES_SOURCE_DIR!")
        sys.exit(1)

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='SpinTires Saves Backup Management Tool')
    parser.add_argument(
        '--log-level',
        action='store',
        choices=['debug', 'info', 'warn', 'error'],
        default='WARN',
        help='Set the log level',
    )
    subparsers = parser.add_subparsers()

    # SAVE
    parser_save = subparsers.add_parser(
        'save',
        help='Save current SpinTires saves directory. Requires that SpinTires is not running.'
    )
    parser_save.add_argument(
        'backup_name',
        help='Name for the backup. If it doesnt exist yet, a new one will be created. If one with the given name exists'
        ' already, a new revision will be created.',
    )
    parser_save.add_argument(
        'revision',
        nargs='?', # make optional
        help='(Optional) Name for the newly created revision of this backup.',
    )
    parser_save.set_defaults(func=command_save)

    # RESTORE
    parser_restore = subparsers.add_parser(
        'restore',
        help='Restore a previously created backup. Requires that SpinTires is not running. Before restoring the given'
        'backup/revision, a backup of the current files is created.'
    )
    parser_restore.add_argument(
        'backup_name',
        help='Name of the backup to restore. Restores the latest revision of the given backup, unless a revision is'
        'also specified.',
    )
    parser_restore.add_argument(
        'revision',
        nargs='?', # make optional
        help='(Optional) Name of the revision of the specified backup to restore.',
    )
    parser_restore.set_defaults(func=command_restore)

    # LIST
    parser_list = subparsers.add_parser('list', help='List available backups and their revisions')
    parser_list.add_argument(
        'backup_name',
        nargs='?', # make optional
        help='(Optional) If specified, only list this backup (but all its revisions)',
    )
    parser_list.add_argument(
        '--include-revisions',
        action='store_true',
        help='List all revisions for all available backups'
    )
    parser_list.add_argument(
        '--include-internal',
        action='store_true',
        help='List all backups (including internal backups)',
    )
    parser_list.set_defaults(func=command_list)

    # DELETE
    parser_delete = subparsers.add_parser('delete', help='Delete a previously created backup (or revision).')
    parser_delete.add_argument(
        'backup_name',
        help='Name of the backup to delete. Will delete all its revisions',
    )
    parser_delete.add_argument(
        'revision',
        nargs='?', # make optional
        help='(Optional) Name of the revision of the specified backup to delete.',
    )
    parser_delete.set_defaults(func=command_delete)

    parsed_args = parser.parse_args()
    if 'func' not in parsed_args:
        parser.print_help()
        sys.exit(1)

    return parsed_args


def command_list(args: argparse.Namespace):
    debug(f'list {args}')

    backups = Backup.load_list_from_path(SAVES_BACKUP_DIR)
    debug(backups)

    for backup in backups:
        if args.backup_name:
            if backup.name == args.backup_name:
                backup.pretty_print(include_revisions=True, even_if_internal=True)
                break
        else:
            backup.pretty_print(include_revisions=args.include_revisions, even_if_internal=args.include_internal)


def command_save(args: argparse.Namespace):
    debug(f'save {args}')

    if is_spintires_running():
        error("SpinTires is still running. Please close the game first, otherwise backups may not be reliable.")
        sys.exit(1)

    backup_name = args.backup_name
    backups = Backup.load_list_from_path(SAVES_BACKUP_DIR)

    backup = Backup.get_from_list_or_create(backups, backup_name, SAVES_BACKUP_DIR)

    if not backup.create_new_revision(args.revision, SAVES_SOURCE_DIR):
        error("Could not create backup!")
        sys.exit(1)


def command_restore(args: argparse.Namespace):
    # TODO: Make another backup of game saves before restore?
    debug(f'restore {args}')

    if is_spintires_running():
        error("SpinTires is still running. Please close the game first, otherwise backups may not be reliable.")
        sys.exit(1)

    backup_name = args.backup_name
    revision_name = args.revision
    backups = Backup.load_list_from_path(SAVES_BACKUP_DIR)

    backup = Backup.get_from_list(backups, backup_name)
    if not backup:
        error("No backup with the given name found. Aborting!")
        sys.exit(1)

    if revision_name:
        revision = backup.get_revision(revision_name)
        if not revision:
            error("No revision with the given name exists. Aborting!")
            sys.exit(1)
    else:
        revision = backup.get_latest_revision()

    debug(f"Restoring revision {revision}")
    if revision:
        internal_autobackup = Backup.get_from_list_or_create(
            backups,
            Backup.INTERNAL_RESTORESAVES_NAME,
            SAVES_BACKUP_DIR
        )
        internal_autobackup.create_new_revision(f'before-restoring_{backup.name}_{revision.name}', SAVES_SOURCE_DIR)
        revision.restore(SAVES_SOURCE_DIR)


def command_delete(args: argparse.Namespace):
    debug(f'delete {args}')

    backup_name = args.backup_name
    revision_name = args.revision
    backups = Backup.load_list_from_path(SAVES_BACKUP_DIR)

    backup = Backup.get_from_list(backups, backup_name)
    if not backup:
        error("No backup with the given name found. Aborting!")
        sys.exit(1)

    if revision_name:
        # delete only the given revision
        revision = backup.get_revision(revision_name)
        if not revision:
            error("No revision with the given name exists. Aborting!")
            sys.exit(1)

        revision.delete()
    else:
        # delete the entire backup
        backup.delete()


def is_spintires_running() -> bool:
    for process in psutil.process_iter():
        try:
            cmdline = process.cmdline()
            if len(cmdline) > 0 and cmdline[0].endswith('SpinTires.exe'):
                return True
        except (psutil.AccessDenied, psutil.ZombieProcess, psutil.NoSuchProcess):
            pass

    return False

if __name__ == '__main__':
    main()
