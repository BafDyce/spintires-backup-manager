# SprinTires Backup Manager

Helps you managing SpinTires backups in order to more easily recover from crashes (esp. in Multiplayer sessions).

## How To

If in doubt, run `python spintires-backup-manager.py --help`.
Before you can use the script, you need to set the following variables within the script:
- `SAVES_SOURCE_DIR` To the path of the directory where SpinTires stores its saves data (the directory which contains
`Config.xml`, `UserSaves`, etc.)
- `SAVES_BACKUP_DIR` The directory where you want the backups to be saved to.

### General

```
usage: spintires-backup-manager.py [-h] [--log-level {debug,info,warn,error}] {save,restore,list,delete} ...

SpinTires Saves Backup Management Tool

positional arguments:
  {save,restore,list,delete}
    save                Save current SpinTires saves directory. Requires that SpinTires is not running.
    restore             Restore a previously created backup. Requires that SpinTires is not running. Before restoring the
                        givenbackup/revision, a backup of the current files is created.
    list                List available backups and their revisions
    delete              Delete a previously created backup (or revision).

optional arguments:
  -h, --help            show this help message and exit
  --log-level {debug,info,warn,error}
                        Set the log level
```

### Subcommand list

```
usage: spintires-backup-manager.py list [-h] [--include-revisions] [--include-internal] [backup_name]

positional arguments:
  backup_name          If specified, only list this backup (but all its revisions)

optional arguments:
  -h, --help           show this help message and exit
  --include-revisions  List all revisions for all available backups
  --include-internal   List all backups (including internal backups)
```

### Subcommand save

```
usage: spintires-backup-manager.py save [-h] backup_name [revision]

positional arguments:
  backup_name  Name for the backup. If it doesnt exist yet, a new one will be created. If one with the given name exists
               already, a new revision will be created.
  revision     (Optional) Name for the newly created revision of this backup.

optional arguments:
  -h, --help   show this help message and exit
```


### Subcommand restore

```
usage: spintires-backup-manager.py restore [-h] backup_name [revision]

positional arguments:
  backup_name  Name of the backup to restore. Restores the latest revision of the given backup, unless a revision isalso
               specified.
  revision     (Optional) Name of the revision of the specified backup to restore.

optional arguments:
  -h, --help   show this help message and exit
```

### Subcommand delete

```
usage: spintires-backup-manager.py delete [-h] backup_name [revision]

positional arguments:
  backup_name  Name of the backup to delete. Will delete all its revisions
  revision     (Optional) Name of the revision of the specified backup to delete.

optional arguments:
  -h, --help   show this help message and exit
```

## License: MIT

```
MIT License

Copyright (c) 2020 Fabian Würfl

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

## Disclaimer

*SpinTires Backup Manager is not affiliated, associated, authorized, endorsed by, or in any way officially connected with SpinTires®, Oovee Ltd., or any of its subsidiaries or its affiliates.*
